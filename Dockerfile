# koalaman/shellcheck:v0.10.0
FROM koalaman/shellcheck@sha256:047a571f02e690c5da4e6b10e9afc37503931534c005ca6192db8ca144d1b01a as shellcheck

# mvdan/shfmt:v3.9.0
FROM mvdan/shfmt@sha256:cb4ca87cc18e52f184a7ba1ae1ef7350b79a2c216ace78a0d24b473e87f0b8f5 as shfmt

FROM docker/compose:alpine-1.29.2 as docker-alpine

FROM alpine:3.20

RUN set -ex; \
    apk add --no-cache \
    bash~=5 \
    curl~=8 \
    nodejs~=20 \
    npm~=10 \
    zip~=3 \
    git~=2 \
    yamllint~=1.35 \
    aws-cli~=2.15 && \
    npm install -g @prantlf/jsonlint@16.0.0 && \
    curl -sSL -o /usr/local/bin/hadolint https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64 && \
    chmod 700 /usr/local/bin/hadolint && \
    rm -rf /root/.npm && \
    apk del --purge npm && \
    rm -rf /var/cache/apk/*

COPY --from=docker-alpine /usr/local/bin/docker-compose /usr/bin/docker-compose
COPY --from=shellcheck /bin/shellcheck /usr/local/bin/shellcheck
COPY --from=shfmt /bin/shfmt /usr/local/bin/shfmt
COPY *.sh /usr/local/bin/
