# Bob

Meet Bob

![Bob the builder](img/Bob_the_builder.jpg)

A Docker container with all necessary binaries to lint, check
format and publish changes.

Some of the included programs:

* jsonlint
* yamllint
* shellcheck
* hadolint
* shfmt
* docker and docker-compose
* aws cli

In order to use `docker`, Bob assumes that you are running the
container using Docker in Docker (or equivalent), or that you map the
external Docker socket internally to the container.
