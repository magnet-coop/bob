#!/usr/bin/env bash

set -eu -o pipefail

TAG="${BITBUCKET_COMMIT:=local}"
TAG="${TAG:0:10}"

docker build \
    -t "magnetcoop/bob:${TAG}" \
    -t "magnetcoop/bob:latest" .
