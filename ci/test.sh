#!/usr/bin/env bash

set -eu -o pipefail

echo "Checking installation..."
hadolint -v
jsonlint -v
shfmt -version
shellcheck -V

echo "Checking source code..."
./all-checks.sh
