#!/usr/bin/env bash

set -eu -o pipefail

TAG="${BITBUCKET_COMMIT:=local}"
TAG="${TAG:0:10}"

docker login --username "${DOCKERHUB_USERNAME}" --password "${DOCKERHUB_PASSWORD}"

docker push "magnetcoop/bob:${TAG}"
