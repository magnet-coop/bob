#!/usr/bin/env bash

set -eu -o pipefail

docker run \
    --rm \
    --volume "$(pwd):/code" \
    --workdir /code \
    magnetcoop/bob:latest "$@"
