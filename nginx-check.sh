#!/usr/bin/env bash

if [[ ! -f "proxy/conf.d/default.conf" ]]; then
    echo "proxy/conf.d/default.conf"
    exit 1
fi

upstream_names=$(awk -F ':' '/proxy_pass/ {print substr($2,3)}' proxy/conf.d/default.conf)
add_hosts=""
for upstream_name in ${upstream_names}; do
    add_hosts="${add_hosts} --add-host=${upstream_name}:127.0.0.1"
done

if [[ -z "${nginx_version}" && -f "proxy/Dockerfile" ]]; then
    nginx_version=$(sed -n 's/FROM nginx:\(.*\)/\1/p' proxy/Dockerfile)
fi

if [[ -z "${nginx_version}" ]]; then
    echo "Unable to detect nginx version, skipping test"
    exit 0
fi

echo "Nginx version: ${nginx_version}"
echo "Upstream name: ${upstream_name}"

# shellcheck disable=SC2086
docker run \
    --rm \
    ${add_hosts} \
    --volume="$(pwd)/proxy/conf.d:/etc/nginx/conf.d" \
    "nginx:${nginx_version}" \
    nginx -t -c /etc/nginx/nginx.conf
