#!/usr/bin/env bash

set -eu -o pipefail

echo "Running hadolint on Dockerfile..."
find . -name Dockerfile -type f -print0 | xargs -0 -r -n1 hadolint
echo "Running jsonlint on JSON files..."
find . -name '*.json' -type f -print0 | xargs -0 -r -n1 jsonlint -q
echo "Running shellcheck on shellscripts..."
find . -name '*.sh' -type f -print0 | xargs -0 -r -n1 shellcheck
echo "Running shfmt on shellscripts..."
find . -name '*.sh' -type f -print0 | xargs -0 -r -n1 -I file shfmt -i 4 -d file
echo "Running yamllint on YAML files..."
yamllint -d relaxed .
echo "Running trailing-whitespace-check.sh on shellscripts, Clojure(Script) files and YAML files"
find . -type f \( -name '*.sh' -o -name '*.clj' -o -name '*.cljs' -o -name '*.cljc' -o -name '*.yaml' -o -name '*.yml' \) -print0 |
    xargs -0 -r -n1 -I file trailing-whitespace-check.sh file

# nginx config check
echo "Checking NGINX configuration..."
if [[ -f "proxy/conf.d/default.conf" && -n "$(command -v docker)" ]]; then
    nginx-check.sh
fi

echo "Done!"
