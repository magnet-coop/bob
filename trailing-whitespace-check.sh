#!/usr/bin/env bash

set -eu -o pipefail

if grep -n -H -E ' +$' "$1"; then
    echo "Trailing whitespace found in file $1"
    exit 1
fi

last_line="$(tail -n 1 "$1")"
if echo "${last_line}" | grep -q -E '^$'; then
    echo "Empty line(s) found at the end of file $1"
    exit 1
fi

exit 0
